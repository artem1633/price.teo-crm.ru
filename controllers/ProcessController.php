<?php

namespace app\controllers;

use app\models\UploadForm;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class ProcessController extends Controller
{
    public function actionResult($fileName)
    {

        $directory = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR;
        $filePath = $directory . $fileName;

        $source = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . $fileName;

        $param = file_exists($filePath) ? [
            'exist' => true,
            'url' => Yii::getAlias('@webDownloads') . DIRECTORY_SEPARATOR . $fileName,
        ] : [
            'exist' => false,
            'source' => file_exists($source),
            'url' => null,
        ];

        return $this->render('result', $param);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {

            if ($model->load(Yii::$app->request->post())) {

                $clearCmd = 'cd ' . Yii::getAlias('@app') . DIRECTORY_SEPARATOR . "&& php yii process/clear";
                exec($clearCmd);

                $model->csvFile = UploadedFile::getInstance($model, 'csvFile');
                if ($model->upload()) {
                    // file is uploaded successfully
                    $source = $model->filePath;
                    $destination = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . $model->fileName;
                    $processCmd = 'cd ' . Yii::getAlias('@app') . DIRECTORY_SEPARATOR . "&& php yii process {$source} {$destination} {$model->increasePercent} &";
                    exec($processCmd);
                    sleep(2);
                    return $this->redirect(['result', 'fileName' => $model->fileName]);
                }
            } else {
                return $this->render('index', [
                    'model' => $model,
                ]);
            }


        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
