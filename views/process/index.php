<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\UploadForm */

$this->title = 'Обработка файла';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="upload-form container">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'id' => 'upload-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-3 control-label'],
        ],
    ]); ?>

    <div class="form-group">
        <?= $form->field($model, 'csvFile')->fileInput(['accept' => '.csv']) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'increasePercent')->textInput(['autofocus' => true]) ?>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Обработать', ['class' => 'btn btn-primary', 'name' => 'process-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
