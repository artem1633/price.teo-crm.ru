<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var bool $exist */
/* @var bool $source */
/* @var string $url */

$this->title = 'Результат обработки файла';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($exist): ?>
        <p>
            Обработка файла завершена.
        </p>

        <?= Html::a('Скачать', $url, ['class' => 'btn btn-primary']); ?>

    <?php else: ?>

        <?php if ($source): ?>
        <p>
            Обработка файла в процессе.
        </p>
        <p>
            Подождите и обновите страницу для получения ссылки на файл.
        </p>
            <?= Html::a('Обновить', 'javascript:history.go(0)', ['class' => 'btn btn-warning']); ?>

        <?php else: ?>

            <p>
                Файл источник отсуствует.
            </p>
            <p>
                Обработанный файл отсуствует.
            </p>
            <p>
                Попробуйсте загрузить файл снова.
            </p>
            <?= Html::a('Загрузить', '/', ['class' => 'btn btn-danger']); ?>
        <?php endif; ?>

    <?php endif; ?>
</div>