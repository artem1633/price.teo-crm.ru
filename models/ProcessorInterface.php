<?php

namespace app\models;


interface ProcessorInterface
{
    /**
     * @param array $row
     * @return array
     */
    public function process($row);
}