<?php

namespace app\models;


use yii\base\Model;

class RowPriceProcessor extends Model implements ProcessorInterface
{
    public $increasePercent = 0.0;

    public function process($row)
    {
        $output = $row;
        $price = floatval($row[4]);
        $newPrice = round($price + $price * ($this->increasePercent / 100), 2);
        $output[4] = $newPrice;
        return $output;
    }


}