<?php

namespace app\models;


use Keboola\Csv\CsvReader;
use Keboola\Csv\CsvWriter;
use Keboola\Csv\Exception;

class FileProcessor
{
    protected $source;
    /**
     * @var ProcessorInterface[]
     */
    protected $rowProcessors = [];
    /**
     * @var CsvReader
     */
    protected $reader;
    /**
     * @var CsvWriter
     */
    protected $writer;
    protected $destination;

    public function __construct($sourceCsvFile)
    {
        $this->source = $sourceCsvFile;
    }

    /**
     * @param ProcessorInterface[] $processors
     */
    public function setProcessors($processors)
    {
        $this->rowProcessors = $processors;
    }

    public function write($destinationCsvFile)
    {
        $this->destination = $destinationCsvFile;

        $this->reader = new CsvReader($this->source,
            ';',
            CsvReader::DEFAULT_ENCLOSURE,
            CsvReader::DEFAULT_ESCAPED_BY,
            1
        );
        $this->writer = new CsvWriter($this->destination, ';');
        $this->writer->writeRow($this->reader->getHeader());

        foreach ($this->reader as $row) {
            try {
                $this->writer->writeRow(
                    $this->processRow($row)
                );
            } catch (Exception $exception) {
                \Yii::error($exception->getMessage());
            }
        }
    }

    protected function processRow($row)
    {
        $output = $row;
        foreach ($this->rowProcessors as $processor) {
            $output = $processor->process($output);
        }
        return $output;
    }
}