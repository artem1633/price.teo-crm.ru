<?php

namespace app\models\jobs;

use app\models\FileProcessor;
use app\models\RowPriceProcessor;
use yii\base\Model;
use yii\queue\JobInterface;

class ProcessCsvFileJob extends Model implements JobInterface
{
    public $source;
    public $destination;
    public $increasePercent = 0.0;

    public function execute($queue)
    {
        $fileProcessor = new FileProcessor($this->source);
        $fileProcessor->setProcessors([
            new RowPriceProcessor(['increasePercent' => $this->increasePercent]),
        ]);
        $fileProcessor->write($this->destination);

        //unlink($this->source);
    }
}