<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    public $increasePercent = 0.0;

    public $csvFile;
    public $filePath = null;
    public $fileName = null;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['csvFile'], 'required'],
            //[['csvFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'csv'],

            ['increasePercent', 'number'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'increasePercent' => 'Процент увеличения цен',
            'csvFile' => 'Файл CSV',
        ];
    }

    public function upload()
    {

        if ($this->validate()) {
            $csvFile = UploadedFile::getInstance($this, 'csvFile');
            $directory = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR;

            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }

            if ($csvFile) {
                $uid = str_replace('.', '', uniqid(time(), true));
                $fileName = $uid . '.' . $csvFile->extension;
                $filePath = $directory . $fileName;
                if ($csvFile->saveAs($filePath)) {
                    $this->filePath = $filePath;
                    $this->fileName = $fileName;
                    return true;
                }
            }
        }
        var_dump($this->errors);
        return false;
    }

}
