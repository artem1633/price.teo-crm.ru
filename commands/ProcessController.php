<?php

namespace app\commands;

use app\models\jobs\ProcessCsvFileJob;
use yii\console\Controller;

class ProcessController extends Controller
{
    public function actionClear()
    {
        $uploads = \Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR;
        $downloads = \Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR;

        $clearUploads = "rm -f {$uploads}*.csv &";
        $clearDownloads = "rm -f {$downloads}*.csv &";

        exec($clearUploads);
        exec($clearDownloads);
    }

    public function actionIndex($source, $destination, $increasePercent)
    {
        if (file_exists($destination)) {
            unlink($destination);
        }

        $job = new ProcessCsvFileJob([
            'source' => $source,
            'destination' => $destination,
            'increasePercent' => $increasePercent,
        ]);

        $job->execute(\Yii::$app->queue);
    }
}
